package se.rocketscien.service.converter;

import se.rocketscien.model.Product;
import se.rocketscien.product.attributes.Article;
import se.rocketscien.product.attributes.Articles;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

public class ProductConverter {

    public Articles convertToArticles(List<Product> products) {
        List<Article> collectedEntries = products.stream()
                .map(product -> {
                    Article entry = new Article();
                    entry.setIdArt(BigInteger.valueOf(product.getIdArt()));
                    entry.setName(product.getName());
                    entry.setCode(product.getCode());
                    entry.setUsername(product.getUsername());
                    entry.setGuid(product.getGuid());

                    return entry;
                })
                .collect(Collectors.toList());

        Articles entries = new Articles();
        entries.getArticle().addAll(collectedEntries);

        return entries;
    }

}
