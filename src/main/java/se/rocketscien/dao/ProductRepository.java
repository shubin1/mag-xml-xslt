package se.rocketscien.dao;

import lombok.RequiredArgsConstructor;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import se.rocketscien.dao.mapper.ProductMapper;
import se.rocketscien.model.Product;

import java.util.List;
import java.util.function.Function;

@RequiredArgsConstructor
public class ProductRepository {

    private final SqlSessionFactory sessionFactory;

    public List<Product> getAllTests() {
        return executeInsideSession(ProductMapper::selectAll);
    }

    public void insertTest(Product product) {
        executeInsideSession((Function<ProductMapper, Void>) mapper -> {
            mapper.insertProduct(product);

            return null;
        });
    }

    public void insertAllTests(List<Product> products) {
        executeInsideSession((Function<ProductMapper, Void>) mapper -> {
            products.forEach(mapper::insertProduct);

            return null;
        });
    }

    public void createTableIfNotExists() {
        executeInsideSession((Function<ProductMapper, Void>) mapper -> {
            mapper.createTableIfNotExists();

            return null;
        });
    }

    public void deleteAllTests() {
        executeInsideSession((Function<ProductMapper, Void>) mapper -> {
            mapper.deleteAll();
            return null;
        });
    }

    private <T> T executeInsideSession(Function<ProductMapper, T> func) {
        try (SqlSession session = sessionFactory.openSession()) {
            ProductMapper mapper = session.getMapper(ProductMapper.class);

            T result = func.apply(mapper);

            session.flushStatements();
            session.commit();
            return result;
        }
    }

}
