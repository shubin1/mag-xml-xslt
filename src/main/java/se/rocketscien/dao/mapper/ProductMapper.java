package se.rocketscien.dao.mapper;

import org.apache.ibatis.annotations.Mapper;
import se.rocketscien.model.Product;

import java.util.List;

@Mapper
public interface ProductMapper {

    void insertProduct(Product product);

    List<Product> selectAll();

    void deleteAll();

    void createTableIfNotExists();
}
