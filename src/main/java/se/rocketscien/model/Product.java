package se.rocketscien.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product {

    private long idArt;

    private String name;

    private String code;

    private String username;

    private String guid;

}
