package se.rocketscien.app;

import lombok.Getter;
import lombok.Setter;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.sqlite.SQLiteConfig;
import org.sqlite.javax.SQLiteConnectionPoolDataSource;
import se.rocketscien.dao.ProductRepository;
import se.rocketscien.dao.mapper.ProductMapper;
import se.rocketscien.model.Product;
import se.rocketscien.service.XMLDocumentService;
import se.rocketscien.service.converter.ProductConverter;

import javax.sql.DataSource;
import java.io.File;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

@Setter
@Getter
public class Starter {

    private static final String SCHEMA_TRANSITION_XSL = "/schema/transition.xsl";
    private static final String SCHEMA_CSV_TRANSITION_XSL = "/schema/csv-transition.xsl";
    private static final String FIRST_FILE_NAME = "out/1.xml";
    private static final String SECOND_FILE_NAME = "out/2.xml";
    private static final String THIRD_FILE_NAME = "out/3.csv";

    private String dbUrl;

    private Long number;

    public void run() throws Exception {
        ProductConverter productConverter = new ProductConverter();
        ProductRepository productRepository = new ProductRepository(acquireSessionFactory());
        XMLDocumentService xmlDocumentService = new XMLDocumentService(productConverter);

        productRepository.createTableIfNotExists();

        File firstXml = new File(FIRST_FILE_NAME);
        File secondXml = new File(SECOND_FILE_NAME);
        File thirdCsv = new File(THIRD_FILE_NAME);

        // Clean up
        productRepository.deleteAllTests();

        // Insert
        List<Product> products = LongStream.range(1, number + 1).boxed()
                .map(i -> new Product(i, "Name", "Code", "Username", "guid"))
                .collect(Collectors.toList());
        productRepository.insertAllTests(products);

        // Read and Marshall
        List<Product> allProducts = productRepository.getAllTests();

        xmlDocumentService.marshallTestsIntoFile(allProducts, firstXml);

        // XSLT transform
        xmlDocumentService.transformFileWithXslTemplateToFile(
                firstXml,
                getClass().getResource(SCHEMA_TRANSITION_XSL),
                secondXml
        );

        xmlDocumentService.transformFileWithXslTemplateToFile(
                secondXml,
                getClass().getResource(SCHEMA_CSV_TRANSITION_XSL),
                thirdCsv);
    }

    private SqlSessionFactory acquireSessionFactory() {
        DataSource dataSource = acquireDataSource(dbUrl);
        JdbcTransactionFactory transactionFactory = new JdbcTransactionFactory();
        Environment main = new Environment("main", transactionFactory, dataSource);

        Configuration configuration = new Configuration(main);
        configuration.addMapper(ProductMapper.class);

        return new SqlSessionFactoryBuilder().build(configuration);
    }

    private DataSource acquireDataSource(String dbUrl) {
        SQLiteConfig sqliteConfig = new SQLiteConfig();
        SQLiteConnectionPoolDataSource sqliteDataSource = new SQLiteConnectionPoolDataSource(sqliteConfig);

        sqliteDataSource.setUrl(dbUrl);

        return sqliteDataSource;
    }

}
