<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fields="http://rocketscien.se/product/fields"
                xmlns:attrs="http://rocketscien.se/product/attributes">
    <xsl:strip-space elements="*"/>
    <xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

    <xsl:template match="/">
        <fields:articles>
            <xsl:apply-templates/>
        </fields:articles>
    </xsl:template>

    <xsl:template match="attrs:article">
        <fields:article>
            <fields:id_art>
                <xsl:value-of select="@id_art"/>
            </fields:id_art>
            <fields:name>
                <xsl:value-of select="@name"/>
            </fields:name>
            <fields:code>
                <xsl:value-of select="@code"/>
            </fields:code>
            <fields:username>
                <xsl:value-of select="@username"/>
            </fields:username>
            <fields:guid>
                <xsl:value-of select="@guid"/>
            </fields:guid>
        </fields:article>
    </xsl:template>
</xsl:stylesheet>